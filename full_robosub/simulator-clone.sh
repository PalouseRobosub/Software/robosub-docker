#!/bin/bash

source /opt/ros/kinetic/setup.bash

pushd ~/ros > /dev/null
pushd src > /dev/null

git clone https://gitlab.com/palouserobosub/software/robosub_simulator.git

popd > /dev/null # Pop src

catkin_make -j8

popd > /dev/null # Pop ~/ros
