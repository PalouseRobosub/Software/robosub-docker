#!/bin/bash
#sudo sh -c 'echo "deb http://robosub.eecs.wsu.edu/repo/ /" > /etc/apt/sources.list.d/robosub.list'
#wget http://robosub.eecs.wsu.edu/repo/repository_key -O - | sudo apt-key add -
#sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu xenial main" > /etc/apt/sources.list.d/ros-latest.list'
#wget https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -O - | sudo apt-key add -
#sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu xenial main" > /etc/apt/sources.list.d/gazebo-latest.list'
#wget http://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add -
#sudo sh -c 'echo "deb https://packagecloud.io/github/git-lfs/ubuntu/ xenial main" > /etc/apt/sources.list.d/github_git-lfs.list'
#wget https://packagecloud.io/github/git-lfs/gpgkey -O - | sudo apt-key add -
#sudo aptitude update
#sudo aptitude install robosub-kinetic robosub-simulator-kinetic git-lfs -y
#git lfs install

#echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc

source ~/.bashrc

mkdir -p ~/ros/src

pushd ~/ros/src > /dev/null
#pushd src > /dev/null

source /opt/ros/kinetic/setup.bash
catkin_init_workspace

#git clone https://github.com/PalouseRobosub/robosub.git
#git clone https://github.com/PalouseRobosub/robosub_simulator.git
#git clone https://github.com/PalouseRobosub/robosub_msgs.git
#popd > /dev/null
#catkin_make
popd > /dev/null
